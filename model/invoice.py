# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv.orm import Model, fields


class Invoice(Model):
    _inherit = 'account.invoice'

    def button_reset_taxes(self, cr, uid, ids, context=None):
        # Attach a hook
        self._set_commission_rules(cr, uid, ids, context=context)
        return super(Invoice, self).button_reset_taxes(cr, uid, ids,
                                                       context=context)

    def _set_commission_rules(self, cr, uid, ids, context=None):
        context = context or {}
        commission_rule_obj = self.pool.get('commission.rule')
        commission_line_obj = self.pool.get('commission.line')
        for inv_id in ids:
            invoice_fields = self.read(
                cr, uid, inv_id, ['partner_id', 'date_invoice',
                                  'amount_untaxed'],
                context=context)
            partner_id = invoice_fields['partner_id'][0]
            date_invoice = (invoice_fields.get('date_invoice', None) or
                            fields.datetime.now())
            amount_untaxed = invoice_fields['amount_untaxed']
            conditions = [
                ('customer_ids', 'in', [partner_id]),
                ('period_from', '<=', date_invoice),
                ('period_to', '>=', date_invoice),
            ]
            commission_rule_ids = commission_rule_obj.search(cr, uid,
                                                             conditions)
            # Delete previous commission lines
            cr.execute("DELETE FROM commission_line WHERE invoice_id=%s",
                       (inv_id,))
            commission_line_obj._set_commission_lines(
                cr, uid, inv_id, amount_untaxed, commission_rule_ids)
        return True

    def _compute_total_commissions(self, cr, uid, ids, field_name, args,
                                   context=None):
        commission_line_obj = self.pool.get('commission.line')
        res = {}
        for inv_id in ids:
            applied_commission_rules = commission_line_obj.search(
                cr, uid, [('invoice_id', '=', inv_id)])
            applied_commission_amounts = commission_line_obj.read(
                cr, uid, applied_commission_rules, ['amount'])
            total_commission_amount = 0.0
            for value in applied_commission_amounts:
                total_commission_amount += value['amount']
            res[inv_id] = total_commission_amount
        return res

    def _get_invoice_line(self, cr, uid, trigger_ids, context=None):
        result = {}
        for line in self.browse(cr, uid, trigger_ids, context=context):
            result[line.invoice_id.id] = True
        return result.keys()

    def _get_commission_lines(self, cr, uid, trigger_ids, context=None):
        res = {}
        commission_line_obj = self.pool.get('commission.line')
        for line in commission_line_obj.browse(
                cr, uid, trigger_ids, context=context):
            res[line.invoice_id.id] = True
        return res.keys()

    _columns = {
        'total_commission_amount': fields.function(
            _compute_total_commissions, field="float",
            string="Total commission amount", store={
                'account.invoice': (
                    lambda self, cr, uid, ids, c={}: ids,
                    ['invoice_line'],
                    20),
                'account.invoice.line': (
                    _get_invoice_line,
                    ['price_unit', 'invoice_line_tax_id', 'quantity',
                     'discount', 'invoice_id'],
                    20),
                'commission.line': (
                    _get_commission_lines,
                    ['amount'],
                    20),
            }),
        'commission_line_ids': fields.one2many(
            'commission.line', 'invoice_id',
            string="Commission amounts for invoices"),
        'commission_rules': fields.related(
            'commission_line_ids', 'commission_rule_id', type='many2one',
            relation='commission.rule', string="Commission Rules"),
    }
