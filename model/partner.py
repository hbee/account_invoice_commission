# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv.orm import Model, fields


class Partner(Model):
    _inherit = 'res.partner'
    _columns = {
        'commission_rule_ids': fields.many2many('commission.rule',
                                                string="Commission Rule"),
        'owned_commission_rule_ids': fields.one2many(
            'commission.rule', 'supplier_id')
    }
