# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv.orm import Model, fields, _


class ComissionRule(Model):
    _name = 'commission.rule'
    _rec_name = 'supplier_id'

    def _check_commission_share(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        if obj.commission_share < 0.0 or obj.commission_share > 100.0:
            return False
        return True

    def _check_period(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        if obj.period_to < obj.period_from:
            return False
        return True

    def _pretty_repr(self, cr, uid, ids, field_names, args, context=None):
        """
        Return commission field formated for percent representation

        Combine period_from and period_to to field representing a period
            e.g. 2014-09-01 - 2015-09-01
        """
        res = dict.fromkeys(ids, None)
        for comm_rule in self.browse(cr, uid, ids, context=context):
            res[comm_rule.id] = dict((fn, 0.0) for fn in field_names)
            commission_share = comm_rule.commission_share
            res[comm_rule.id]['commission_repr'] = commission_share
            res[comm_rule.id]['period_repr'] = "{} - {}".format(
                comm_rule.period_from, comm_rule.period_to)
        return res

    _columns = {
        'commission_share': fields.float('Comission share %', required=True),
        'name': fields.char('Description', required=True),
        'period_from': fields.date('Period Start',
                                   required=True),
        'period_to': fields.date('Period End', required=True),
        'customer_ids': fields.many2many('res.partner', string='Customers',
                                         domain=[('customer', '=', True)],
                                         required=True,),
        'commission_repr': fields.function(
            _pretty_repr, type="float", string="Commission amount %",
            multi="prettyrepr", readonly=True),
        'period_repr': fields.function(
            _pretty_repr, type="char", string="Period", multi="prettyrepr",
            readonly=True),
        'supplier_id': fields.many2one('res.partner', 'Supplier',
                                       domain=[('supplier', '=', True)],
                                       required=True),
        'invoices': fields.many2many(
            'account.invoice', 'invoices', 'commission_rules',
            string="Invoices"),
    }

    _constraints = [
        (_check_commission_share, 'Commission share must be between 0 and 100',
         ['commission_share']),
        (_check_period, 'Invalid commission period',
         ['period_from', 'period_to']),
    ]

    def copy(self, cr, uid, id, default=None, context=None):
        default = {}
        commission_rule = self.read(cr, uid, id, context=context)
        default.update({
            'name': _("%s (copy)") % (commission_rule.get('name') or ''),
        })
        return super(ComissionRule, self).copy(
            cr, uid, id, default, context=context)


class CommissionInvoiceAmount(Model):
    _name = 'commission.line'

    def _set_commission_lines(self, cr, uid, invoice_id, amount_untaxed,
                              commission_rule_ids, context=None):
        context = context or {}
        commission_rule_obj = self.pool.get('commission.rule')
        commission_rule_br = commission_rule_obj.browse(
            cr, uid, commission_rule_ids, context=context)
        for commission_rule in commission_rule_br:
            amount = amount_untaxed * commission_rule.commission_share / 100
            self.create(cr, uid, {
                'amount': amount,
                'invoice_id': invoice_id,
                'commission_rule_id': commission_rule.id,
            }, context=context)

    _columns = {
        'amount': fields.float('Commission amount'),
        'invoice_id': fields.many2one('account.invoice',
                                      'Commission invoice'),
        'commission_rule_id': fields.many2one('commission.rule',
                                              'Commission rule'),
        'supplier': fields.related('commission_rule_id', 'supplier_id',
                                   type='many2one', relation="res.partner",
                                   string="Supplier", readonly=True),
        'period_repr': fields.related('commission_rule_id', 'period_repr',
                                      type="char", string="Period",
                                      readonly=True),
        'commission_repr': fields.related('commission_rule_id',
                                          'commission_repr', type="float",
                                          string="Commissions %",
                                          readonly=True),
        'commissions': fields.related('commission_rule_id', 'commission_share',
                                      type='float', string="Commission Share",
                                      readonly=True),
    }
