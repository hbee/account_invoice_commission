# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.tests.common import BaseCase
from openerp import SUPERUSER_ID
from datetime import date, timedelta
from openerp.osv import fields
import uuid


class BaseTestCase(BaseCase):

    def setUp(self):
        self.cr = self.cursor()
        self.uid = SUPERUSER_ID
        cr, uid = self.cr, self.uid
        context = {}

        self.commission_rule_obj = self.registry('commission.rule')
        self.commission_line_obj = self.registry(
            'commission.line')
        self.partner_obj = self.registry('res.partner')
        self.product_obj = self.registry('product.product')
        self.invoice_obj = self.registry('account.invoice')
        self.invoice_line_obj = self.registry('account.invoice.line')

        self.one_rule_customer_id = self._create_customer()
        self.multi_rule_customer_id = self._create_customer()

        demo_suppliers = self.partner_obj.search(
            cr, uid, [('supplier', '=', True)], limit=2)
        self.first_supplier_id = demo_suppliers[0]
        self.second_supplier_id = demo_suppliers[1]

        self.product_ids = self.product_obj.search(
            cr, uid, [('active', '=', True)], limit=5)

        # Create commission rules
        # Get todays date
        date_today = date.today()
        date_previous_year = date_today - timedelta(days=365)
        date_next_year = date_today + timedelta(days=365)
        date_after_two_years = date_today + timedelta(days=2 * 365)

        # fields.date.context_today acquires resources with self.pool.get
        # see /osv/fields.py:310
        # method, which does not exist in BaseCase class. It should call
        # self.registry instead.
        # Fix: make a pointer self.pool.get -> self.registry
        self.pool = self
        self.pool.get = self.registry

        # Generate commission rules valid through this year
        # For one rule customer
        self._create_commission_rule(
            self.one_rule_customer_id, self.second_supplier_id,
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_previous_year),
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_next_year))
        # For multi rule customer
        self._create_commission_rule(
            self.multi_rule_customer_id, self.first_supplier_id,
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_previous_year),
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_next_year))
        self._create_commission_rule(
            self.multi_rule_customer_id, self.first_supplier_id,
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_previous_year),
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_next_year))
        # Create a commission rule valid only in the upcoming year
        self._create_commission_rule(
            self.multi_rule_customer_id, self.first_supplier_id,
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_next_year),
            fields.date.context_today(self, cr, uid, context=context,
                                      timestamp=date_after_two_years))

    def tearDown(self):
        self.cr.close()

    def _create_invoice_for_customer(self, customer_id):
        cr, uid = self.cr, self.uid
        payable_account = self.partner_obj.read(
            cr, uid, customer_id, ['property_account_payable'])
        account_id = payable_account['property_account_payable'][0]
        invoice_values = {
            'partner_id': customer_id,
            'invoice_line': self._gen_invoice_lines(self.product_ids),
            'account_id': account_id,
        }
        invoice_id = self.invoice_obj.create(cr, uid, invoice_values)
        # Recompute taxes of the invoice
        self.invoice_obj.button_reset_taxes(cr, uid, [invoice_id])
        return invoice_id

    def _create_commission_rule(self, customer_id, supplier_id,
                                period_from, period_to):
        cr, uid = self.cr, self.uid
        commission_share = 10.0
        commission_rule_values = {
            'name': uuid.uuid1().hex,
            'commission_share': commission_share,
            'period_from': period_from,
            'period_to': period_to,
            'customer_ids': [(6, 0, [customer_id])],
            'supplier_id': supplier_id,
        }
        return self.commission_rule_obj.create(
            cr, uid, commission_rule_values)

    def _gen_invoice_lines(self, product_ids):
        cr, uid = self.cr, self.uid
        products = self.product_obj.browse(cr, uid, product_ids)
        return [(0, 0, {
            'product_id': product.id,
            'name': product.name_template,
            'price_unit': product.lst_price,
        }) for product in products]

    def _create_customer(self):
        vals = {
            'name': uuid.uuid1().hex
        }
        return self.partner_obj.create(
            self.cr, self.uid, vals)
