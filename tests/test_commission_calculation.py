# import unittest2
from .basetestcase import BaseTestCase

# TODO test if finds a correct number of applied commission rules
# TODO test if calculates commission amount correctly if only one rule
# is applied
# TODO test if calculates commission amount correctly if several rules
# are applied


class TestCommissionCalculation(BaseTestCase):
    def setUp(self):
        super(TestCommissionCalculation, self).setUp()

    def tearDown(self):
        super(TestCommissionCalculation, self).tearDown()

    def test_one_rule_commission_calculation(self):
        cr, uid = self.cr, self.uid
        invoice_id = self._create_invoice_for_customer(
            self.one_rule_customer_id)
        invoice_fields = self.invoice_obj.read(
            cr, uid, invoice_id, ['amount_untaxed', 'total_commission_amount'])
        commission_amount = invoice_fields['total_commission_amount']
        amount_untaxed = invoice_fields['amount_untaxed']
        self.assertEquals(commission_amount, amount_untaxed*0.1,
                          "Commission invoice amount calculated from one rule "
                          "incorrect")

    def test_commission_rule_date_filtering(self):
        cr, uid = self.cr, self.uid
        invoice_id = self._create_invoice_for_customer(
            self.multi_rule_customer_id)
        invoice_fields = self.invoice_obj.read(
            cr, uid, invoice_id, ['commission_rules'])
        n_rules = len(invoice_fields['commission_rules'])
        self.assertEquals(n_rules, 2,
                          "Commission rule filtering by date is incorrect")

    def test_multiple_rule_commission_calculation(self):
        cr, uid = self.cr, self.uid
        invoice_id = self._create_invoice_for_customer(
            self.multi_rule_customer_id)
        invoice_fields = self.invoice_obj.read(
            cr, uid, invoice_id, ['amount_untaxed', 'total_commission_amount'])
        commission_amount = invoice_fields['total_commission_amount']
        amount_untaxed = invoice_fields['amount_untaxed']
        self.assertEqual(commission_amount, amount_untaxed*0.2,
                         "Commission invoice amount calculated from multiple "
                         "rules is incorrect")

    def test_multiple_invoice_updates_do_not_generate_extra_lines(self):
        cr, uid = self.cr, self.uid
        invoice_id = self._create_invoice_for_customer(
            self.multi_rule_customer_id)
        # Update invoice multiple times for a total of 3 times
        self.invoice_obj.button_reset_taxes(cr, uid, [invoice_id])
        self.invoice_obj.button_reset_taxes(cr, uid, [invoice_id])
        # Should still contain only two commission lines
        n_commission_lines = self.commission_line_obj.search(
            cr, uid, [('invoice_id', '=', invoice_id)], count=True)
        self.assertEquals(n_commission_lines, 2,
                          "Duplicate commission lines are inserted "
                          "on each update")
