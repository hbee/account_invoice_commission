# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Account invoice commissions',
    'version': '0.2',
    'author': 'HacBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
Concept
=======

Each supplier can have a list of commission rules that can be applied to a number of
customers. Each commission rule contains a period through which the commission rule
is valid. Upon invoice creation/update, commission rules applied to the customer are attached
to the invoice and can be viewed on a separate tab page. Commission rules attached to the invoice
can only be updated when invoice is in validated yet(i.e. in state "draft")

Workflows
=========

1) Create commission rules with appropriate customers, supplier and period of interest
2) For newly created invoices all current applied rules will be automatically set
3) For existing "draft" invoices to update current commission rule list one should click "Edit"
   button in invoice form view, and click on link "(update)" which refilters commission rules and
   recomputes commission amount

Menus / Windows
===============

**Commission Rules** menu under **Suppliers** in  **Accounting** module

Setup Windows
-------------

Commission rules
~~~~~~~~~~~~~~~~

* **Commission Rules:** Multiple Rules can be created. The amount of calculation will be based on untaxed Invoice Amount.
* **Commission Share:** Part of invoice amount calculated for commissions. E.g. 0.1 (stands for 10%)
* **Period Start:** Start of period for which the customer rule is valid
* **Period End:** End of period for which the rule is valid
* **Supplier:** The Owner of the commission rule and person which gets the commission
* **Customers:** Customers which are affected by the commission rule

    The commission amount from a particular rule is calculated by: [Invoice Subtotal]*[Commission Share]
    Total commission amount is a sum of all commission amounts from commission rules applied to the invoice

""",
    'depends': [
        'account',
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/account_invoice_commision.xml',
    ],
    'test': [
    ],
    'installable': True,
    'application': False,
}
