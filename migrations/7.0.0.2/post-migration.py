# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
from openerp import pooler, SUPERUSER_ID
from openerp.tools import float_round


def migrate(cr, installed_version):
    pool = pooler.get_pool(cr.dbname)
    commission_rule_obj = pool.get('commission.rule')
    commission_rule_ids = commission_rule_obj.search(
        cr, SUPERUSER_ID, [])
    for commission_rule in commission_rule_obj.browse(
            cr, SUPERUSER_ID, commission_rule_ids):
        commission_share = float_round(
            commission_rule.commission_share, precision_digits=2) * 100
        commission_rule.write({'commission_share': commission_share})
